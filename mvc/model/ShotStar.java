package _08final.mvc.model;

public class ShotStar extends ShotTemplate {
    public ShotStar() {
        speed = 12;
        power = 4;
        expire = 100;
        team = Movable.Team.FOE;
        radius = 10;

        int[][] outline = {
                {-3,-5},
                {0, 5},
                {3, -5},
                {-5, 3},
                {5, 3}
        };

        int[][] hitbox = {
                {-3,-5},
                {3,-5},
                {5,3},
                {0,5},
                {-3,5}
        };

        spriteOutline = outline;
        hitboxOutline = hitbox;
    }
}
