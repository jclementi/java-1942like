package _08final.mvc.model;

import com.sun.org.apache.bcel.internal.classfile.ConstantInteger;

import java.awt.*;
import java.util.ArrayList;

/**
 * designed to build things out of multiple sprites
 *
 * has the list of sprites it contains and where
 * to place each one
 */
public class MultiSprite implements Movable{
    public ArrayList<ConstituentSprite> sprites;
    private Point center;
    private int orientation;
    private double spin;
    private Team team;
    private double deltaX, deltaY;
    private Color color;
    private int radius;

    public ArrayList<ConstituentSprite> getSprites() { return sprites; }
    public void setSprites(ArrayList<ConstituentSprite> cs) { sprites = cs; }
    public Point getCenter() { return center; }
    public void setCenter(Point c) { center = c; }
    public int getOrientation() { return orientation; }
    public void setOrientation(int o) { orientation = o; }
    public double getSpin() { return spin; }
    public void setSpin(double s) { spin = s; }
    public Team getTeam() { return team; }
    public void setTeam(Team t) { team = t; }
    public double getDeltaX() { return deltaX; }
    public void setDeltaX(double dx) { deltaX = dx; }
    public double getDeltaY() { return deltaY; }
    public void setDeltaY(double dy) { deltaY = dy; }
    public Color getColor() { return color; }
    public void setColor(Color c) { color = c; }
    public int getRadius() { return 0; }

    /* change the centerpoint and orientation of every sprite
     * in the multisprite
     *
     * the center of each constituent sprite is the placement relative
     * to the center of the multisprite
     */
    public void move() {
        orientation += spin;

        int ctrX, ctrY;
        center.x += deltaX;
        center.y += deltaY;
        for(ConstituentSprite cs : sprites) {
            ctrX = center.x + (int) Math.round(
                    Math.cos(-Math.toRadians(orientation))*cs.getOffset().x +
                    Math.sin(-Math.toRadians(orientation))*cs.getOffset().y);
            ctrY = center.y + (int) Math.round(
                    Math.sin(Math.toRadians(orientation))*cs.getOffset().x +
                    Math.cos(Math.toRadians(orientation))*cs.getOffset().y);
            cs.getSprite().setCenter(new Point(ctrX, ctrY));
            cs.getSprite().setOrientation(orientation);
        }
    }

    public void draw(Graphics g) {
        for(ConstituentSprite cs: sprites) {
            cs.getSprite().draw(g);
        }
    }

    public void addSprite(Sprite s, Point ctr) {
        s.setTeam(team);
        s.setOrientation(orientation);
        s.setColor(color);
        int ctrX = center.x + (int) Math.round(
                Math.cos(Math.toRadians(orientation))*ctr.x +
                        Math.sin(Math.toRadians(orientation))*ctr.y);
        int ctrY = center.y + (int) Math.round(
                Math.sin(Math.toRadians(orientation))*ctr.x +
                        Math.cos(Math.toRadians(orientation))*ctr.y);
        s.setCenter(new Point(ctrX, ctrY));
        sprites.add(new ConstituentSprite(s, ctr, false));
    }

    public MultiSprite() {
        sprites = new ArrayList<>();
        center = new Point(0, 0);
        orientation = 0;
        team = Movable.Team.FOE;
        color = Color.white;
    }

    public MultiSprite(ArrayList<Sprite> ss, ArrayList<Point> ps, Movable.Team t, Point ctr, Color c) {
        sprites = new ArrayList<>();
        center = ctr;
        team = t;
        color = c;
        for (int i = 0; i < ss.size(); i++) {
            addSprite(ss.get(i), ps.get(i));
        }
    }
}

