package _08final.mvc.model;

public class EnemySpriteAntenna1 extends EnemySpriteTemplate {
    public EnemySpriteAntenna1() {
        int[][] bodyPts = {{4, 0}, {4, 1}, {-4, 1}, {-4, 0}};
        bodyOutline = bodyPts;
        hitboxOutline = bodyPts;
        radius = 30;
        health = 1;
    }
}
