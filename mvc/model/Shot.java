package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

/**
 * A shot is a more generalized bullet. It can be fired by either team.
 * It can have a distinct flight path and hitbox. Subclasses define the
 * shots from each weapon in the game.
 */
public class Shot implements Movable {
    MultiSprite representation;
    private double speed;
    private int power = 1;
    private int expire = 20;

    public MultiSprite getRepresentation() { return representation; }
    public double getPower() { return power; }
    public double getSpeed() { return speed; }
    public void setSpeed(double s) { speed = s; }
    public int getExpire() { return expire; }
    public void setExpire(int e) { expire = e; }

    public Shot(ShotTemplate st, Point ctr) {
        setSpeed(st.speed);
        setExpire(st.expire);
        Sprite slug = new Sprite();
        slug.setRadius(st.radius);
        slug.setStrength(st.power);
        slug.setHealth(1);

        ArrayList<Point>bodyPts = new ArrayList<>();
        for (int[] i: st.spriteOutline) {
            bodyPts.add(new Point(i[0], i[1]));
        }

        ArrayList<Point>hbPts = new ArrayList<>();
        for (int[] i: st.hitboxOutline) {
            hbPts.add(new Point(i[0], i[1]));
        }

        slug.assignPolarPoints(bodyPts);
        slug.assignPolarHitbox(hbPts);

        ArrayList<Sprite> ss = new ArrayList<>();
        ArrayList<Point> ps = new ArrayList<>();
        ss.add(slug);
        ps.add(new Point(0, 0));
        representation = new MultiSprite(ss, ps, Movable.Team.FOE, ctr, Color.ORANGE);
        representation.setTeam(st.team);

        CommandCenter c = CommandCenter.getInstance();
        double diff_x = c.getFalcon().getCenter().getX() - representation.getCenter().getX();
        double diff_y = c.getFalcon().getCenter().getY() - representation.getCenter().getY();
        double dist = Math.sqrt((diff_x * diff_x)+(diff_y*diff_y));
        double normalized_dx = diff_x/dist;
        double normalized_dy = diff_y/dist;

        representation.setDeltaX(normalized_dx * speed);
        representation.setDeltaY(normalized_dy * speed);
        representation.setOrientation((int)Math.toDegrees(Math.atan(normalized_dy/normalized_dx)));

    }

    public void move() {
        if (getExpire() < 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
            return;
        }

        setExpire(getExpire() - 1);
        representation.move();
    }
    public Point getCenter() { return representation.getCenter(); }
    public int getRadius() { return representation.getRadius(); }
    public Team getTeam() { return representation.getTeam(); }
    public void draw(Graphics g) { representation.draw(g); }
}



