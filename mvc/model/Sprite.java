package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

public class Sprite implements Movable {
	private Point pntCenter;
	private double dDeltaX, dDeltaY;
	private Dimension dim;

	private Team mTeam;

	private int nRadius;

	private int nOrientation;
	private int nExpiry;
	private Color col;
	public double[] dLengths;
	public double[] dDegrees;

	//fade value for fading in and out
	private int nFade;

	//these are used to draw the polygon. You don't usually need to interface with these
	private Point[] pntCoords; //an array of points used to draw polygon
	private int[] nXCoords;
	private int[] nYCoords;

	private Point[] hbPntCoords;
	private int[] hbXCoords;
	private int[] hbYCoords;
	private double[] hbDegrees;
	private double[] hbLengths;
	private Polygon hitbox;
	private int health;
	private int strength;

	private int flashTimes;
	private int flashDuration;
	private int flashElapsed;

	// new collision detection, health system
	public int[] getHbXCoords() { return hbXCoords; }
	public void setHbXCoords(int[] hbxc) { hbXCoords = hbxc; }
	public int[] getHbYCoords() { return hbYCoords; }
	public void setHbYCoords(int[] hbyc) { hbYCoords = hbyc; }
	public double[] getHbDegrees() { return hbDegrees; }
	public void setHbDegrees(double[] hbd) { hbDegrees = hbd; }
	public double[] getHbLengths() { return hbLengths; }
	public void setHbLengths(double[] hbl) { hbLengths = hbl; }
	public Polygon getHitbox() { return hitbox; }
	public void setHitbox(Polygon p) { hitbox = p; }
	public int getHealth() { return health; }
	public void setHealth(int h) { health = h; }
	public int getStrength() { return strength; }
	public void setStrength(int s) { strength = s; }
	public int getFlashTimes() { return flashTimes; }
	public void setFlashTime(int f) { flashTimes = f; }
	public int getFlashDuration() { return flashDuration; }
	public void setFlashDuration(int f) { flashDuration = f; }
	public int getFlashElapsed() { return flashElapsed; }
	public void setFlashElapsed(int f) { flashElapsed = f; }


	public void flash(int times, int duration) {
		flashTimes = times;
		flashDuration = duration;
		flashElapsed = duration;
	}
	@Override
	public Team getTeam() {
		//default
	  return mTeam;
	}

	public void setTeam(Team team){
		mTeam = team;
	}
	public void move() {

		Point pnt = getCenter();
		double dX = pnt.x + getDeltaX();
		double dY = pnt.y + getDeltaY();
		setCenter(new Point((int) dX, (int) dY));
/*
		//this just keeps the sprite inside the bounds of the frame
		if (pnt.x > getDim().width) {
			setCenter(new Point(1, pnt.y));
		} else if (pnt.x < 0) {
			setCenter(new Point(getDim().width - 1, pnt.y));
		} else if (pnt.y > getDim().height) {
			setCenter(new Point(pnt.x, 1));
		} else if (pnt.y < 0) {
			setCenter(new Point(pnt.x, getDim().height - 1));
		} else {
		}
*/
	}

	public Sprite() {
	//you can override this and many more in the subclasses
		setDim(Game.DIM);
		setColor(Color.white);
		setCenter(new Point(Game.R.nextInt(Game.DIM.width),
				Game.R.nextInt(Game.DIM.height)));
	}
	public void setExpire(int n) { nExpiry = n; }
	public double[] getLengths() {
		return this.dLengths;
	}
	public void setLengths(double[] dLengths) {
		this.dLengths = dLengths;
	}
	public double[] getDegrees() {
		return this.dDegrees;
	}
	public void setDegrees(double[] dDegrees) {
		this.dDegrees = dDegrees;
	}
	public Color getColor() {
		return col;
	}
	public void setColor(Color col) { this.col = col;}

	public int points() {
		//default is zero
		return 0;
	}

	public int getExpire() {
		return nExpiry;
	}
	public int getOrientation() {
		return nOrientation;
	}
	public void setOrientation(int n) {
		nOrientation = n;
	}
	public void setDeltaX(double dSet) {
		dDeltaX = dSet;
	}
	public void setDeltaY(double dSet) {
		dDeltaY = dSet;
	}
	public double getDeltaY() {	return dDeltaY;	}
	public double getDeltaX() {
		return dDeltaX;
	}
	public int getRadius() {
		return nRadius;
	}
	public void setRadius(int n) { nRadius = n; }
	public Dimension getDim() {
		return dim;
	}
	public void setDim(Dimension dim) {
		this.dim = dim;
	}
	public Point getCenter() {
		return pntCenter;
	}
	public void setCenter(Point pntParam) {
		pntCenter = pntParam;
	}
	public void setYcoord(int nValue, int nIndex) {
		nYCoords[nIndex] = nValue;
	}
	public void setXcoord(int nValue, int nIndex) {
		nXCoords[nIndex] = nValue;
	}
	public int getYcoord( int nIndex) {
		return nYCoords[nIndex];// = nValue;
	}
	public int getXcoord( int nIndex) {
		return nXCoords[nIndex];// = nValue;
	}
	public int[] getXcoords() {
		return nXCoords;
	}
	public int[] getYcoords() {
		return nYCoords;
	}
	public void setXcoords( int[] nCoords) {
		 nXCoords = nCoords;
	}
	public void setYcoords(int[] nCoords) {
		 nYCoords =nCoords;
	}
	protected double hypot(double dX, double dY) {
		return Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
	}

	
	//utility function to convert from cartesian to polar
	//since it's much easier to describe a sprite as a list of cartesean points
	//sprites (except Asteroid) should use the cartesean technique to describe the coordinates
	//see Falcon or Bullet constructor for examples
	protected double[] convertToPolarDegs(ArrayList<Point> pntPoints) {

	   double[] dDegs = new double[pntPoints.size()];

		int nC = 0;
		for (Point pnt : pntPoints) {
			dDegs[nC++]=(Math.toDegrees(Math.atan2(pnt.y, pnt.x))) * Math.PI / 180 ;
		}
		return dDegs;
	}
	//utility function to convert to polar
	protected double[] convertToPolarLens(ArrayList<Point> pntPoints) {

		double[] dLens = new double[pntPoints.size()];

		//determine the largest hypotenuse
		double dL = 0;
		for (Point pnt : pntPoints)
			if (hypot(pnt.x, pnt.y) > dL)
				dL = hypot(pnt.x, pnt.y);

		int nC = 0;
		for (Point pnt : pntPoints) {
			if (pnt.x == 0 && pnt.y > 0) {
				dLens[nC] = hypot(pnt.x, pnt.y) / dL;
			} else if (pnt.x < 0 && pnt.y > 0) {
				dLens[nC] = hypot(pnt.x, pnt.y) / dL;
			} else {
				dLens[nC] = hypot(pnt.x, pnt.y) / dL;
			}
			nC++;
		}

		// holds <thetas, lens>
		return dLens;

	}

	protected void assignPolarPoints(ArrayList<Point> pntCs) {
		setDegrees(convertToPolarDegs(pntCs));
		setLengths(convertToPolarLens(pntCs));

	}

	// for hitboxes
	public void assignPolarHitbox(ArrayList<Point> pts) {
		setHbDegrees(convertToPolarDegs(pts));
		setHbLengths(convertToPolarLens(pts));
		hbXCoords = new int[hbDegrees.length];
		hbYCoords = new int[hbDegrees.length];
		hbPntCoords = new Point[hbDegrees.length];

		for (int nC = 0; nC < hbDegrees.length; nC++) {
			hbXCoords[nC] = (int) (getCenter().x + getRadius()
					* hbLengths[nC]
					* Math.sin(Math.toRadians(getOrientation()) + hbDegrees[nC]));
			hbYCoords[nC] = (int) (getCenter().y - getRadius()
					* hbLengths[nC]
					* Math.cos(Math.toRadians(getOrientation()) + hbDegrees[nC]));
			hbPntCoords[nC] = new Point(hbXCoords[nC], hbYCoords[nC]);
		}

		setHitbox(new Polygon(hbXCoords, hbYCoords, hbDegrees.length));
	}

	@Override
    public void draw(Graphics g) {
        nXCoords = new int[dDegrees.length];
        nYCoords = new int[dDegrees.length];
        //need this as well
        pntCoords = new Point[dDegrees.length];

        for (int nC = 0; nC < dDegrees.length; nC++) {
            nXCoords[nC] =    (int) (getCenter().x + getRadius() 
                            * dLengths[nC] 
                            * Math.sin(Math.toRadians(getOrientation()) + dDegrees[nC]));
            nYCoords[nC] =    (int) (getCenter().y - getRadius()
                            * dLengths[nC]
                            * Math.cos(Math.toRadians(getOrientation()) + dDegrees[nC]));
            pntCoords[nC] = new Point(nXCoords[nC], nYCoords[nC]);
        }

		g.setColor(getColor());
		if (flashTimes > 0) {
			if ((flashTimes % 2) == 1) { g.setColor(Color.RED); }
			flashElapsed--;
			if (flashElapsed <= 0) {
				flashElapsed = flashDuration;
				flashTimes--;
			}
		}
		g.drawPolygon(getXcoords(), getYcoords(), dDegrees.length);
		g.setColor(getColor());

		hbXCoords = new int[hbDegrees.length];
		hbYCoords = new int[hbDegrees.length];
		hbPntCoords = new Point[hbDegrees.length];

		for (int nC = 0; nC < hbDegrees.length; nC++) {
			hbXCoords[nC] = (int) (getCenter().x + getRadius()
					* hbLengths[nC]
					* Math.sin(Math.toRadians(getOrientation()) + hbDegrees[nC]));
			hbYCoords[nC] = (int) (getCenter().y - getRadius()
					* hbLengths[nC]
					* Math.cos(Math.toRadians(getOrientation()) + hbDegrees[nC]));
			hbPntCoords[nC] = new Point(hbXCoords[nC], hbYCoords[nC]);
		}

		setHitbox(new Polygon(hbXCoords, hbYCoords, hbDegrees.length));
		//g.setColor(Color.RED);
		//g.drawPolygon(hbXCoords, hbYCoords, hbDegrees.length);
    }
    

	public Point[] getObjectPoints() {
		return pntCoords;
	}
	
	public void setObjectPoints(Point[] pntPs) {
		 pntCoords = pntPs;
	}
	
	public void setObjectPoint(Point pnt, int nIndex) {
		 pntCoords[nIndex] = pnt;
	}

	public int getFadeValue() {
		return nFade;
	}

	public void setFadeValue(int n) {
		nFade = n;
	}

}
