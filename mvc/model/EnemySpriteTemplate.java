package _08final.mvc.model;

public abstract class EnemySpriteTemplate {
    public int[][] bodyOutline;
    public int[][] hitboxOutline;
    public int radius;
    public int health;
    public int strength;
}
