package _08final.mvc.model;

public class PathRamStraightC extends PathTemplate {
    public PathRamStraightC() {
        int[][] dests = {
                {700, -100},
                {700, 100},
                {700, 130},
                {700, 140},
                {700, 150},
                {700, 1200}
        };
        ps = dests;
        os = new int[] {90, 90, 90, 90, 90, 90};
        ss = new int[] {100, 10, 15, 20, 25, 30};
        fs = new boolean[]{false, false, false, false, false, false};
    }
}
