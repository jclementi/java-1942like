package _08final.mvc.model;

import java.awt.*;
import java.nio.file.Paths;

public class EnemyQuickShooter extends EnemyTemplate {
    public EnemyQuickShooter(PathTemplate[] ps) {
        sprites = new EnemySpriteTemplate[]{new EnemySpriteQuickShooter()};
        offsets = new Point[]{new Point(0, 0)};
        orientation = 180;
        spin = 0;
        reward = 50;
        paths = ps;
        weapon = new Weapon(new ShotB());
    }
}
