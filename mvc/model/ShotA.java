package _08final.mvc.model;

import java.awt.*;

public class ShotA extends ShotTemplate {
    public ShotA() {
        speed = 8;
        power = 3;
        expire = 200;
        team = Movable.Team.FOE;
        radius = 10;

        int[][] outline = {
                {-1,-1},
                {0, 1},
                {1, -1}
        };

        spriteOutline = outline;
        hitboxOutline = outline;
    }
}
