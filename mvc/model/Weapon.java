package _08final.mvc.model;

import java.awt.*;

public class Weapon {
    ShotTemplate template;

    public Weapon(ShotTemplate t) {
        template = t;
    }

    public void fire(Point p) {
        Shot s = new Shot(template, p);
        CommandCenter.getInstance().getOpsList().enqueue(s, CollisionOp.Operation.ADD);
    }
}
