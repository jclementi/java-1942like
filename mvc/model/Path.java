package _08final.mvc.model;

import java.awt.*;
import java.util.LinkedList;

/**
 * Path contains a queue of Orders that direct movement for enemies.
 * The queue is a simple linked list, which will be a FIFO queue.
 */

public class Path {
    LinkedList<Order> orders;

    public Path(PathTemplate pt) {
        orders = new LinkedList<Order>();
        for (int i = 0; i < pt.ps.length; i++) {
            orders.add(new Order(
                    new Point(pt.ps[i][0], pt.ps[i][1]),
                    pt.ss[i],
                    pt.os[i],
                    pt.fs[i]
            ));
        }
    }

    public Path(PathTemplate[] pts) {
        orders = new LinkedList<Order>();
        if (pts == null) { return; }
        for (PathTemplate pt : pts) {
            for (int i = 0; i < pt.ps.length; i++) {
                orders.add(new Order(
                        new Point(pt.ps[i][0], pt.ps[i][1]),
                        pt.ss[i],
                        pt.os[i],
                        pt.fs[i]
                ));
            }
        }
    }

    public Path(Point[] points, double[] speeds, int[] os, boolean[] fs) {
        orders = new LinkedList<Order>();
        for (int i = 0; i < points.length; i++) {
            orders.add(new Order(points[i], speeds[i], os[i]));
        }
    }

    public Path(Point[] ps, double[] ss, int[] os) {
        orders = new LinkedList<Order>();
        for (int i = 0; i < ps.length; i++) {
            orders.add(new Order(ps[i], ss[i], os[i]));
        }
    }

    public Path(Point[] ps, double[] ss, boolean[] fs) {
        orders = new LinkedList<Order>();
        for (int i = 0; i < ps.length; i++) {
            orders.add(new Order(ps[i], ss[i], fs[i]));
        }
    }
}
