package _08final.mvc.model;

import java.awt.*;

public class Order {
    Point destination;
    double speed;
    int orientation;
    boolean fire;

    public Order(Point d, double s, int o, boolean f) {
        destination = d;
        speed = s;
        orientation = o;
        fire = f;
    }

    public Order(Point d, double s, int o) {
        destination = d;
        speed = s;
        orientation = o;
        fire = false;
    }

    public Order(Point d, double s, boolean f) {
        destination = d;
        speed = s;
        orientation = 90;
        fire = f;
    }
}
