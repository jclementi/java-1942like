package _08final.mvc.model;

public class PathRamStraightB extends PathTemplate {
    public PathRamStraightB() {
        int[][] dests = {
                {400, -100},
                {400, 100},
                {400, 130},
                {400, 140},
                {400, 150},
                {400, 1200}
        };
        ps = dests;
        os = new int[] {90, 90, 90, 90, 90, 90};
        ss = new int[] {100, 10, 15, 20, 25, 30};
        fs = new boolean[]{false, false, false, false, false, false};
    }
}
