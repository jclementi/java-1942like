package _08final.mvc.model;

public class EnemySpriteQuickShooter extends EnemySpriteTemplate {
    public EnemySpriteQuickShooter() {
        int[][] bodyPts = {
                {0, 7},
                {1, 0},
                {4, 3},
                {3, -1},
                {0, -2},
                {-3, -1},
                {-4, 3},
                {-1, 0}
        };

        int[][] hbPts = {
                {0, 7},
                {4, 3},
                {3, -1},
                {-3, -1},
                {-4, 3}
        };
        bodyOutline = bodyPts;
        hitboxOutline = hbPts;
        radius = 30;
        health = 2;
    }
}