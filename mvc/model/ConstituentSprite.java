package _08final.mvc.model;

import java.awt.*;

/** used in conjunction with Multisprite
 *
 */
public class ConstituentSprite {
    private Sprite sprite;
    private Point offset; // relative to conglomerate sprite

    /*
    constituent sprites can be marked as primary
    if they are, then when they are destroyed, the entire multisprite
    is also destroyed. If a non-primary constituent sprite is destroyed,
    the rest stick around.
     */
    private boolean isPrimary;
    private boolean isDead = false;

    public void setSprite(Sprite s) { sprite = s; }
    public Sprite getSprite() {
        return sprite;
    }
    public void setOffset(Point p) { offset = p; }
    public Point getOffset() {
        return offset;
    }
    public void setPrimary(boolean p) { isPrimary = p; }
    public boolean isPrimary() { return isPrimary; }
    public void setDead(boolean d) { isDead = d; }
    public boolean isDead() { return isDead; }

    public ConstituentSprite(Sprite s, Point o, boolean p) {
        sprite = s;
        offset = o;
        isPrimary = p;
        isDead = false;
    }

}
