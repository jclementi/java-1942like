package _08final.mvc.model;

public class PathOffscreen extends PathTemplate {
    public PathOffscreen() {
        int[][] dests = {
                {500, -100}
        };
        ps = dests;
        os = new int[]{90};
        ss = new int[]{20};
        fs = new boolean[]{false};
    }
}
