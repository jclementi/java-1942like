package _08final.mvc.model;

import java.awt.*;

public class EnemyBoss extends EnemyTemplate {
    public EnemyBoss(PathTemplate[] ps) {
        sprites = new EnemySpriteTemplate[]{
                new EnemyBossMain(),
                new EnemyBossArmA(),
                new EnemyBossArmB()
        };

        offsets = new Point[] {
                new Point(0, 0),
                new Point(0, -100),
                new Point(0, 100)
        };
        orientation = 180;
        spin = 0;
        reward = 500;
        paths = ps;
        weapon = new Weapon(new ShotStar());
    }
}
