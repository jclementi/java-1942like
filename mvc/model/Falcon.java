package _08final.mvc.model;

import _08final.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;


public class Falcon extends Sprite {

	// ==============================================================
	// FIELDS 
	// ==============================================================
	
	private final double THRUST = 0;

	final int DEGREE_STEP = 7;
	final double BOUNDARY = 2.25;
	
	private boolean bShield = false;
	private boolean bFlame = false;
	private boolean bProtected; //for fade in and out
	
	private boolean bThrusting = false;
	private boolean bTurningRight = false;
	private boolean bTurningLeft = false;
	
	private int nShield;
			
	private final double[] FLAME = { 23 * Math.PI / 24 + Math.PI / 2,
			Math.PI + Math.PI / 2, 25 * Math.PI / 24 + Math.PI / 2 };

	private int[] nXFlames = new int[FLAME.length];
	private int[] nYFlames = new int[FLAME.length];

	private Point[] pntFlames = new Point[FLAME.length];


	// ==============================================================
	// ADDED PROPERTIES
	// ==============================================================
	private int speed_x = 0;
	private int speed_y = 0;
	private int accel_x = 0;
	private int accel_y = 0;
	private double resistance;
	private int health = 10;
	private int maxHealth = 10;

	public int getMaxHealth() { return maxHealth; }
	public int getHealth() { return health; }
	public void setHealth(int h) { health = h; }


	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public Falcon() {
		super();
		setTeam(Team.FRIEND);
		ArrayList<Point> pntCs = new ArrayList<Point>();
		
		// Robert Alef's awesome falcon design
		pntCs.add(new Point(0,9));
		pntCs.add(new Point(-1, 6));
		pntCs.add(new Point(-1,3));
		pntCs.add(new Point(-4, 1));
		pntCs.add(new Point(4,1));
		pntCs.add(new Point(-4,1));

		pntCs.add(new Point(-4, -2));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-4, -2));

		pntCs.add(new Point(-10, -8));
		pntCs.add(new Point(-5, -9));
		pntCs.add(new Point(-7, -11));
		pntCs.add(new Point(-4, -11));
		pntCs.add(new Point(-2, -9));
		pntCs.add(new Point(-2, -10));
		pntCs.add(new Point(-1, -10));
		pntCs.add(new Point(-1, -9));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -10));
		pntCs.add(new Point(2, -10));
		pntCs.add(new Point(2, -9));
		pntCs.add(new Point(4, -11));
		pntCs.add(new Point(7, -11));
		pntCs.add(new Point(5, -9));
		pntCs.add(new Point(10, -8));
		pntCs.add(new Point(4, -2));

		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(1, -9));
		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(4,-2));


		pntCs.add(new Point(4, 1));
		pntCs.add(new Point(1, 3));
		pntCs.add(new Point(1,6));
		pntCs.add(new Point(0,9));

		assignPolarPoints(pntCs);

		ArrayList<Point> hitboxOutline = new ArrayList<>();
		Integer[][] hbPts = {
				{0, 9},
				{-10, -8},
				{-7, -11},
				{7, -11},
				{10, -8},
		};
		for (Integer[] i: hbPts) {
			hitboxOutline.add(new Point(i[0],i[1]));
		}
		assignPolarHitbox(hitboxOutline);
		setColor(Color.white);

		setCenter(new Point(500, 800));
		setOrientation(270);
		
		//this is the size of the falcon
		setRadius(35);

		setProtected(true);
		bThrusting = false;
		setFadeValue(0);
		resistance = .3;
		accel_x = 0;
		accel_y = 0;
		speed_x = 0;
		speed_y = 0;
	}
	
	
	// ==============================================================
	// METHODS 
	// ==============================================================

	@Override
	public void move() {
		super.move();
		speed_x += accel_x;
		speed_y += accel_y;

		speed_x += -1*speed_x*resistance;
		speed_y += -1*speed_y*resistance;

		// push the ship away from the edges
		if (getCenter().getX() < getRadius() * BOUNDARY) {
			speed_x += 10;
		}
		if (getCenter().getX() > (Game.DIM.getWidth() - getRadius() * BOUNDARY)) {
			speed_x -= 10;
		}
		if (getCenter().getY() < getRadius() * BOUNDARY) {
			speed_y += 10;
		}
		if (getCenter().getY() > (Game.DIM.getHeight() - getRadius() * BOUNDARY)) {
			speed_y -= 10;
		}

		setDeltaX(speed_x);
		setDeltaY(speed_y);

		//implementing the fadeInOut functionality - added by Dmitriy
		if (getProtected()) {
			setFadeValue(getFadeValue() + 3);
		}
		if (getFadeValue() == 30) {
			setProtected(false);
		}
	} //end move

	public void rotateLeft() {
		bTurningLeft = true;
	}

	public void rotateRight() {
		bTurningRight = true;
	}

	public void stopRotating() {
		bTurningRight = false;
		bTurningLeft = false;
	}

	public void thrustOn() {
		bThrusting = true;
	}

	public void thrustOff() {
		bThrusting = false;
		bFlame = false;
	}

	private int adjustColor(int nCol, int nAdj) {
		if (nCol - nAdj <= 0) {
			return 0;
		} else {
			return nCol - nAdj;
		}
	}

	public void drawShipWithColor(Graphics g, Color col) {
		super.draw(g);
		g.setColor(col);
		g.drawPolygon(getXcoords(), getYcoords(), dDegrees.length);
	}


	public void setProtected(boolean bParam) {
		if (bParam) {
			setFadeValue(0);
		}
		bProtected = bParam;
	}


	public boolean getProtected() {return bProtected;}

	public void accelerateForward() {
		accel_y = -10;
	}
	public void accelerateBack() {
		accel_y = 10;
	}
	public void accelerateLeft() {
		accel_x = -10;
	}
	public void accelerateRight() {
		accel_x = 10;
	}

	public void killAccelerationForward() {
		accel_y = Math.min(accel_y + 10, 10);
	}
	public void killAccelerationBack() {
		accel_y = Math.max(accel_y - 10, -10);
	}
	public void killAccelerationLeft() {
		accel_x = Math.min(accel_x + 10, 10);
	}
	public void killAccelerationRight() {
		accel_x = Math.max(accel_x - 10, -10);
	}


} //end class
