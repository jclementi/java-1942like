package _08final.mvc.model;

import java.awt.*;

public class EnemyTriangleShooter extends EnemyTemplate {
    public EnemyTriangleShooter(PathTemplate[] ps) {
        sprites = new EnemySpriteTemplate[2];
        sprites[0] = new EnemySpriteBody1();
        sprites[1] = new EnemySpriteAntenna1();
        offsets = new Point[2];
        offsets[0] = new Point(0, 0);
        offsets[1] = new Point(60, -15);
        orientation = 0;
        spin = 0;
        weapon = new Weapon(new ShotB());
        paths = ps;
    }
}
