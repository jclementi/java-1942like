package _08final.mvc.model;

public class PathRamJuke extends PathTemplate {
    public PathRamJuke() {
        int[][] dests = {
                {200, -100},

                {200, 100},
                {300, 100},
                {250, 100},

                {350, 100},
                {350, 130},
                {350, 140},
                {350, 150},
                {350, 1200}
        };
        ps = dests;
        os = new int[] {90, 90, 90, 90, 90, 90, 90, 90, 90};
        ss = new int[] {100, 10, 10, 10, 10, 15, 20, 25, 30};
        fs = new boolean[]{false, false, false, false,
                false, false, false, false, false};
    }
}
