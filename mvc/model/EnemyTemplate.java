package _08final.mvc.model;

import java.awt.*;

public abstract class EnemyTemplate {
    public EnemySpriteTemplate[] sprites;
    public Point[] offsets;
    public int orientation;
    public double spin;
    public PathTemplate[] paths;
    public int reward;
    public Weapon weapon;
}
