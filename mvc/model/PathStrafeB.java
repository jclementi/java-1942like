package _08final.mvc.model;

public class PathStrafeB extends PathTemplate {
    public PathStrafeB() {
        int[][] dests = {
                {200, 500},
                {400, 500},
                {600, 500},
                {800, 500},

                {800, 600},
                {600, 600},
                {400, 600},
                {200, 600}
        };
        ps = dests;
        os = new int[] {
                90, 90, 90, 90,
                90, 90, 90, 90
        };
        ss = new int[] {
                5, 5, 5, 5,
                5, 5, 5, 5
        };
        fs = new boolean[] {
                true, true, true, true,
                true, true, true, true
        };
    }
}
