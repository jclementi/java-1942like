package _08final.mvc.model;

import java.awt.*;

public class EnemyRam extends EnemyTemplate {
    public EnemyRam(PathTemplate[] ps) {
        sprites = new EnemySpriteTemplate[]{new EnemySpriteRam()};
        offsets = new Point[]{new Point(0, 0)};
        orientation = 180;
        spin = 0;
        paths = ps;
        reward = 200;
        weapon = new Weapon(new ShotA());
    }
}
