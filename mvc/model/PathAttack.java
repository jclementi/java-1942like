package _08final.mvc.model;

public class PathAttack extends PathTemplate {
    public PathAttack() {
        int[][] dests = {
                {100, -100},
                {100, 600},
                {300, 200},
                {300, 600},
                {600, 200},
                {600, -100}
        };
        ps = dests;
        os = new int[]{90, 90, 90, 90, 90, 270};
        ss = new int[]{100, 20, 8, 20, 8, 20};
        fs = new boolean[]{false, true, false, true, false, false};
    }
}
