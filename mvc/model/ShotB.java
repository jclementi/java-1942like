package _08final.mvc.model;

import java.awt.*;

public class ShotB extends ShotTemplate {
    public ShotB() {
        speed = 20;
        power = 1;
        expire = 50;
        team = Movable.Team.FOE;
        radius = 4;

        int[][] outline = {
                {-1,-1},
                {-1, 1},
                {1, 1},
                {1, -1}
        };

        spriteOutline = outline;
        hitboxOutline = outline;
    }
}
