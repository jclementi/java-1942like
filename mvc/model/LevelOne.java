package _08final.mvc.model;

import java.util.LinkedList;

public class LevelOne extends LevelScript {
    public LevelOne() {
        es = new LinkedList<EnemySpawnOrder>();
        PathTemplate[] p1 = new PathTemplate[]{
                new PathAttack()
        };
        EnemyTemplate ts = new EnemyQuickShooter(p1);

        PathTemplate[] p2 = new PathTemplate[]{
                new PathAttackB()
        };
        EnemyTemplate ts2 = new EnemyTriangleShooter(p2);

        EnemySpawnOrder[] esa = new EnemySpawnOrder[20];

        esa[0] = new EnemySpawnOrder(ts, 0);
        esa[1] = new EnemySpawnOrder(ts, 20);
        esa[2] = new EnemySpawnOrder(ts, 40);
        esa[3] = new EnemySpawnOrder(ts, 60);
        esa[4] = new EnemySpawnOrder(ts, 80);
        esa[5] = new EnemySpawnOrder(ts2, 200);
        esa[6] = new EnemySpawnOrder(ts2, 220);
        esa[7] = new EnemySpawnOrder(ts2, 240);
        esa[8] = new EnemySpawnOrder(ts2, 260);
        esa[9] = new EnemySpawnOrder(ts2, 280);
        esa[10] = new EnemySpawnOrder(ts, 400);
        esa[11] = new EnemySpawnOrder(ts, 420);
        esa[12] = new EnemySpawnOrder(ts, 440);
        esa[13] = new EnemySpawnOrder(ts, 460);
        esa[14] = new EnemySpawnOrder(ts, 480);
        esa[15] = new EnemySpawnOrder(ts2, 600);
        esa[16] = new EnemySpawnOrder(ts2, 620);
        esa[17] = new EnemySpawnOrder(ts2, 640);
        esa[18] = new EnemySpawnOrder(ts2, 660);
        esa[19] = new EnemySpawnOrder(ts2, 680);

        for (EnemySpawnOrder eso : esa) {
            es.add(eso);
        }
    }
}
