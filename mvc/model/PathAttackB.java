package _08final.mvc.model;

public class PathAttackB extends PathTemplate {
    public PathAttackB() {
        int[][] dests = {
                {900, -100},
                {900, 600},
                {700, 200},
                {700, 600},
                {400, 200},
                {400, -200}
        };
        ps = dests;
        os = new int[]{90, 90, 90, 90, 90, 270};
        ss = new int[]{100, 20, 8, 20, 8, 20};
        fs = new boolean[]{false, true, false, true, false, false};
    }
}