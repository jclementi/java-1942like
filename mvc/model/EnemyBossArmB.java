package _08final.mvc.model;

public class EnemyBossArmB extends EnemySpriteTemplate {
    public EnemyBossArmB() {
        bodyOutline = new int[][] {
                {0, -4},
                {-4, -2},
                {-4, 6},
                {-1, 10},
                {-2, 5},
                {-2, 2},
                {-0, 0}
        };

        hitboxOutline = new int[][] {
                {0, -4},
                {-4, -2},
                {-4, 6},
                {-1, 10},
                {0, 0}
        };

        radius = 100;
        health = 20;
        strength = 2;
    }
}
