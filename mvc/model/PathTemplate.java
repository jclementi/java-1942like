package _08final.mvc.model;

/**
 * template for building paths
 * ps coordinate information
 * os holds orientation information
 * ss holds speed information
 * fs holds a boolean that indicates whether or not the ship
 * should fire its weapon when it reaches the end of the path
 */
public abstract class PathTemplate {
    public int[][] ps;
    public int[] os;
    public int[] ss;
    public boolean[] fs;
}
