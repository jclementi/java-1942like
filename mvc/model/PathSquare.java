package _08final.mvc.model;

import java.awt.*;

public class PathSquare extends PathTemplate {
    public PathSquare() {
        int[][] dests = {
                {100, -100},
                {100, 500},
                {500, 500},
                {500, 100}
        };
        ps = dests;
        os = new int[]{0, 0, 0, 0};
        ss = new int[]{100, 30, 10, 30};
        fs = new boolean[]{false, true, true, false};

    }
}
