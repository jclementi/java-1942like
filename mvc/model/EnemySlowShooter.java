package _08final.mvc.model;

import java.awt.*;

public class EnemySlowShooter extends EnemyTemplate {
    public EnemySlowShooter(PathTemplate[] ps) {
        sprites = new EnemySpriteTemplate[]{new EnemySpriteSlowShooter()};
        offsets = new Point[]{new Point(0, 0)};
        orientation = 180;
        spin = 0;
        reward = 20;
        paths = ps;
        weapon = new Weapon(new ShotStar());
    }
}
