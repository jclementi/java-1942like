package _08final.mvc.model;

public class EnemySpawnOrder {
    public EnemyTemplate et;
    public int time;

    public EnemySpawnOrder(EnemyTemplate e, int t) {
        et = e;
        time = t;
    }
}
