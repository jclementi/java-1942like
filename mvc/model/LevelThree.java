package _08final.mvc.model;

import java.util.LinkedList;

public class LevelThree extends LevelScript {
    public LevelThree() {
        es = new LinkedList<EnemySpawnOrder>();

        PathTemplate[] bossPath = new PathTemplate[]{
                new PathAttack(),
                new PathStrafe(),
                new PathAttackB(),
                new PathStrafe(),
                new PathAttack(),
                new PathStrafe(),
                new PathAttackB(),
                new PathStrafe(),
                new PathStrafe(),
                new PathStrafe()
        };
        EnemyTemplate boss = new EnemyBoss(bossPath);

        PathTemplate[] ramPathA = new PathTemplate[] { new PathRamStraight() };
        PathTemplate[] ramPathB = new PathTemplate[] { new PathRamStraightB() };
        PathTemplate[] ramPathC = new PathTemplate[] { new PathRamStraightC() };
        PathTemplate[] ramPathJuke = new PathTemplate[] { new PathRamJuke() };
        EnemyTemplate ramA = new EnemyRam(ramPathA);
        EnemyTemplate ramB = new EnemyRam(ramPathB);
        EnemyTemplate ramC = new EnemyRam(ramPathC);
        EnemyTemplate ramJ = new EnemyRam(ramPathJuke);

        PathTemplate[] slowpath = new PathTemplate[] {
                new PathOffscreen(),
                new PathStrafe(),
                new PathStrafe(),
                new PathStrafe(),
                new PathOffscreen()
        };

        PathTemplate[] slowpathB = new PathTemplate[] {
                new PathOffscreen(),
                new PathStrafeB(),
                new PathStrafeB(),
                new PathStrafeB(),
                new PathOffscreen()
        };

        EnemyTemplate ssA = new EnemySlowShooter(slowpath);
        EnemyTemplate ssB = new EnemySlowShooter(slowpathB);

        EnemySpawnOrder[] esa = new EnemySpawnOrder[] {
                new EnemySpawnOrder(ssA, 0),
                new EnemySpawnOrder(ssA, 40),
                new EnemySpawnOrder(ssA, 80),
                new EnemySpawnOrder(ssA, 120),
                new EnemySpawnOrder(ssB, 200),
                new EnemySpawnOrder(ssB, 240),
                new EnemySpawnOrder(ssB, 280),
                new EnemySpawnOrder(ssB, 320),
                new EnemySpawnOrder(ramA, 400),
                new EnemySpawnOrder(ramB, 400),
                new EnemySpawnOrder(ramC, 400),
                new EnemySpawnOrder(ssB, 400),
                new EnemySpawnOrder(ssB, 440),
                new EnemySpawnOrder(ssB, 480),
                new EnemySpawnOrder(ssB, 520),
                new EnemySpawnOrder(ssA, 520),
                new EnemySpawnOrder(ssA, 560),
                new EnemySpawnOrder(ssA, 600),
                new EnemySpawnOrder(ssA, 640),
                new EnemySpawnOrder(ssA, 680),
                new EnemySpawnOrder(ssA, 720),
                new EnemySpawnOrder(ssB, 720),
                new EnemySpawnOrder(ssB, 760),
                new EnemySpawnOrder(ssB, 800),
                new EnemySpawnOrder(ssB, 840),
                new EnemySpawnOrder(ramJ, 950),
                new EnemySpawnOrder(ramA, 1050),
                new EnemySpawnOrder(ramB, 1050),
                new EnemySpawnOrder(ramC, 1050),
                new EnemySpawnOrder(boss, 1250),
                new EnemySpawnOrder(ssA, 1300),
                new EnemySpawnOrder(ssB, 1300),
                new EnemySpawnOrder(ssA, 1340),
                new EnemySpawnOrder(ssB, 1340),
                new EnemySpawnOrder(ssA, 1400),
                new EnemySpawnOrder(ssB, 1400),
                new EnemySpawnOrder(ssA, 1440),
                new EnemySpawnOrder(ssB, 1440)
        };


        for (EnemySpawnOrder eso : esa) {
            es.add(eso);
        }
    }
}
