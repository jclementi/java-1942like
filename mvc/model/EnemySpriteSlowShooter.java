package _08final.mvc.model;

public class EnemySpriteSlowShooter extends EnemySpriteTemplate {
    public EnemySpriteSlowShooter() {
        int[][] bodyPts = {
                {0,7},
                {2,4},
                {6,3},
                {7,-3},
                {3,-1},
                {0,-3},
                {-3,-1},
                {-7,-3},
                {-6,3},
                {-2,4}
        };

        int[][] hbPts = {
                {0,7},
                {6,3},
                {7,-3},
                {-7,-3},
                {-6,3}
        };
        bodyOutline = bodyPts;
        hitboxOutline = hbPts;
        radius = 30;
        health = 5;
    }
}
