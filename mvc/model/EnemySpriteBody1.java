package _08final.mvc.model;

public class EnemySpriteBody1 extends EnemySpriteTemplate {
    public EnemySpriteBody1() {
        int[][] bodyPts = {
                {0,0},
                {2, 0},
                {1, 3}
        };

        bodyOutline = bodyPts;
        hitboxOutline = bodyPts;
        radius = 50;
        health = 2;
    }
}
