package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;

public abstract class ShotTemplate {

    // properties of the shot
    public double speed;
    public int power;
    public int expire;
    public Movable.Team team;

    // properties of the multisprite
    public int radius;
    int[][] spriteOutline;
    int[][] hitboxOutline;

}
