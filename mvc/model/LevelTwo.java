package _08final.mvc.model;

import java.util.LinkedList;

public class LevelTwo  extends LevelScript {
    public LevelTwo() {
        es = new LinkedList<EnemySpawnOrder>();

        PathTemplate[] pram = new PathTemplate[]{
                new PathRamStraight()
        };

        PathTemplate[] pramb = new PathTemplate[]{
                new PathRamStraightB()
        };
        PathTemplate[] pramc = new PathTemplate[]{
                new PathRamStraightC()
        };

        PathTemplate[] pa = new PathTemplate[]{
                new PathAttack()
        };

        PathTemplate[] pb = new PathTemplate[]{
                new PathAttackB()
        };

        PathTemplate[] pc = new PathTemplate[]{
                new PathOffscreen(),
                new PathStrafe(),
                new PathOffscreen()
        };

        EnemyTemplate ts1 = new EnemySlowShooter(pc);
        EnemyTemplate ts2 = new EnemyQuickShooter(pa);
        EnemyTemplate ts3 = new EnemyQuickShooter(pb);
        EnemyTemplate ram = new EnemyRam(pram);
        EnemyTemplate ramb = new EnemyRam(pramb);
        EnemyTemplate ramc = new EnemyRam(pramc);


        EnemySpawnOrder[] esa = new EnemySpawnOrder[]{
                new EnemySpawnOrder(ram, 100),
                new EnemySpawnOrder(ts2, 200),
                new EnemySpawnOrder(ts2, 230),
                new EnemySpawnOrder(ts2, 260),
                new EnemySpawnOrder(ts2, 290),
                new EnemySpawnOrder(ram, 400),
                new EnemySpawnOrder(ts3, 400),
                new EnemySpawnOrder(ts3, 430),
                new EnemySpawnOrder(ts3, 460),
                new EnemySpawnOrder(ts3, 490),
                new EnemySpawnOrder(ramc, 600),
                new EnemySpawnOrder(ts1, 600),
                new EnemySpawnOrder(ramb, 620),
                new EnemySpawnOrder(ts1, 630),
                new EnemySpawnOrder(ram, 640),
                new EnemySpawnOrder(ts1, 660),
                new EnemySpawnOrder(ts1, 690)
        };

        for (EnemySpawnOrder eso : esa) {
            es.add(eso);
        }
    }
}
