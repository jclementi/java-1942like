package _08final.mvc.model;

public class EnemyBossMain extends EnemySpriteTemplate {

    public EnemyBossMain() {
        bodyOutline = new int[][] {
                {0, -5},
                {8, -4},
                {8, 0},
                {4, 2},
                {1, 0},
                {0, 4},
                {-1, 0},
                {-4,2},
                {-8, 0},
                {-8, -4}
        };

        hitboxOutline = new int[][] {
                {0, -5},
                {8, -4},
                {8, 0},
                {0, 4},
                {-8, 0},
                {-8, -4}
        };

        radius = 100;
        health = 50;
        strength = 2;
    }
}
