package _08final.mvc.model;

public class PathRamStraight extends PathTemplate {
    public PathRamStraight() {
        int[][] dests = {
                {200, -100},
                {200, 100},
                {200, 130},
                {200, 140},
                {200, 150},
                {200, 1200}
        };
        ps = dests;
        os = new int[] {90, 90, 90, 90, 90, 90};
        ss = new int[] {100, 10, 15, 20, 25, 30};
        fs = new boolean[]{false, false, false, false, false, false};
    }

}
