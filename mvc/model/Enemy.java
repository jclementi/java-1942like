package _08final.mvc.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * an enemy has a multisprite that describes how it looks, paths
 * that describe how it moves, and a weapon
 */
public class Enemy implements Movable {
    MultiSprite representation;
    Weapon weapon;
    Path path;
    // ArrayList<FirePattern> attack;
    long reward = 10;

    public MultiSprite getRepresentation() { return representation; }
    private void setRepresentation(MultiSprite ms) { representation = ms; }

    public Weapon getWeapon() { return weapon; }
    public void setWeapon(Weapon w) { weapon = w; }
    // movable methods

    // read from path list
    public void move() {
        if (path.orders.isEmpty()) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
            return;
        }

        // check the destination against the current position
        // if they're equal, remove it, and begin moving to the next destination
        Point center = representation.getCenter();
        Point dest = path.orders.getFirst().destination;
        if (center.equals(dest)) {
            if (path.orders.getFirst().fire) { weapon.fire(getCenter()); }
            path.orders.remove(0);
            this.move();
            return;
        }

        // move toward the next destination
        double speed = path.orders.getFirst().speed;
        int orient = path.orders.getFirst().orientation;
        representation.setOrientation(orient);

        double dx, dy;
        double dist_x = dest.getX() - center.getX();
        double dist_y = dest.getY() - center.getY();

        // if the speed would have us overshoot destination, travel exactly to destination instead
        if (speed >= Math.sqrt(dist_x*dist_x + dist_y*dist_y)) {
            representation.setDeltaX(dist_x);
            representation.setDeltaY(dist_y);
            representation.move();
            return;
        }

        double angle = Math.atan(dist_y/dist_x);
        dx = speed * Math.cos(angle);
        dy = speed * Math.sin(angle);

        // make sure the deltas are pointed in the correct direction
        // atan can't distinguish angles pi radians apart
        if ((dist_x * dx) < 0) { dx = -dx; }
        if ((dist_y * dy) < 0) { dy = -dy; }
        representation.setDeltaX(dx);
        representation.setDeltaY(dy);

        representation.move();
    }

    public void draw(Graphics g) { representation.draw(g); }
    public Point getCenter() { return new Point(representation.getCenter().x, representation.getCenter().y); }
    public int getRadius() { return representation.getRadius(); }
    public Team getTeam() { return representation.getTeam(); }

    /* loop through constituent sprites, remove any with <=0 health */
    public void resolveDamage() {
        MultiSprite rep = getRepresentation();
        ArrayList<ConstituentSprite> new_cs = new ArrayList<>();
        for (ConstituentSprite cs : rep.getSprites()) {
            if (cs.getSprite().getHealth() > 0) {
                continue;
            }
            if (cs.isPrimary()) {
                CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
                CommandCenter.getInstance().addScore(reward);
                CommandCenter.getInstance().getOpsList().enqueue(
                        new Debris(cs.getSprite().getCenter(),
                                cs.getSprite().getDeltaX(),
                                cs.getSprite().getDeltaY(),
                                cs.getSprite().getRadius(), 10),
                        CollisionOp.Operation.ADD);
                return;
            }
            cs.setDead(true);
        }

        for (ConstituentSprite cs : rep.getSprites()) {
            if (!cs.isDead()) { new_cs.add(cs); }
            else {
                CommandCenter.getInstance().getOpsList().enqueue(
                        new Debris(cs.getSprite().getCenter(),
                                cs.getSprite().getDeltaX(),
                                cs.getSprite().getDeltaY(),
                                cs.getSprite().getRadius(), 10),
                            CollisionOp.Operation.ADD);
            }
        }

        getRepresentation().setSprites(new_cs);
        /*rep.getSprites().removeIf(new Predicate<ConstituentSprite>() {
            @Override
            public boolean test(ConstituentSprite cs) {
                return cs.isDead();
            }
        });*/
    }
    // constructor
    public Enemy(EnemyTemplate et) {
        // build sprites
        reward = et.reward;
        representation = new MultiSprite();
        representation.setColor(Color.LIGHT_GRAY);
        representation.setCenter(new Point(0, -200)); // start sprites off-screen

        for (int j = 0; j < et.sprites.length; j++) {
            ArrayList<Point> hitboxOutline = new ArrayList<>();
            for(int[] i : et.sprites[j].hitboxOutline) {
                hitboxOutline.add(new Point(i[0], i[1]));
            }

            ArrayList<Point> spriteOutline = new ArrayList<>();
            for (int[] i: et.sprites[j].bodyOutline) {
                spriteOutline.add(new Point(i[0], i[1]));
            }

            Sprite s = new Sprite();
            s.assignPolarHitbox(hitboxOutline);
            s.assignPolarPoints(spriteOutline);
            s.setRadius(et.sprites[j].radius);
            s.setHealth(et.sprites[j].health);
            s.setStrength(et.sprites[j].strength);
            representation.addSprite(s, new Point(et.offsets[j].x, et.offsets[j].y));
        }

        representation.getSprites().get(0).setPrimary(true);
        representation.setOrientation(et.orientation);
        representation.setSpin(et.spin);

        weapon = et.weapon;

        path = new Path(et.paths);
        /*Sprite body = new Sprite();
        ArrayList<Point> outline = new ArrayList<>();
        Integer[][] pts = {
                {0, 0},
                {2, 0},
                {1, 3}};

        for (Integer[] i : pts) {
            outline.add(new Point(i[0],i[1]));
        }
        body.assignPolarPoints(outline);
        body.assignPolarHitbox(outline);
        body.setRadius(80);
        body.setHealth(2);

        Sprite gun = new Sprite();
        ArrayList<Point> gunOutline = new ArrayList<>();
        Integer[][] gunpts = {
                {-1, 10},
                {-1, -10},
                {1, -10},
                {1, 10}
        };

        for (Integer[] i: gunpts) {
            gunOutline.add(new Point(i[0],i[1]));
        }

        gun.assignPolarPoints(gunOutline);
        gun.assignPolarHitbox(gunOutline);
        gun.setRadius(20);
        gun.setHealth(1);

        // put sprites together
        ArrayList<Sprite> sprites = new ArrayList<>();
        ArrayList<Point> centers = new ArrayList<>();

        sprites.add(body);
        centers.add(new Point(0, 0));
        sprites.add(gun);
        centers.add(new Point(200, 80));*/


        // build path
       /* Point[] ps = new Point[4];
        double[] ss = new double[4];
        int[] os = new int[4];
        boolean[] fs = new boolean[4];

        ps[0] = new Point(400, -100);
        ps[1] = new Point(0, 100);
        ps[2] = new Point(200,200);
        ps[3] = new Point(200, 300);

        ss[0] = 80.0;
        ss[1] = 80.0;
        ss[2] = 10.0;
        ss[3] = 10.0;

        os[0] = 0;
        os[1] = 0;
        os[2] = 0;
        os[3] = 45;

        fs[0] = false;
        fs[1] = true;
        fs[2] = true;
        fs[3] = false;

        paths = new ArrayList<Path>();
        paths.add(new Path(ps, ss, os, fs));
        weapon = new Weapon(new ShotA());*/
    }

    public void fire() {
        weapon.fire(getCenter());
    }
}
