package _08final.mvc.model;

import java.awt.*;

public class Debris extends Sprite {
    private int size;
    private int expire;
    private Team team = Team.DEBRIS;

    public Team getTeam() { return team; }
    public void setTeam(Team t) { team = t; }
    public Debris(Point c, double dx, double dy, int s, int e) {
        setCenter(c);
        setDeltaX(dx);
        setDeltaY(dy);
        size = s;
        expire = e;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(Color.getHSBColor((float)Math.random(), (float)1, (float)1));
        g.fillOval(getCenter().x, getCenter().y, size, size);
        size--;
    }

    @Override
    public void move() {
        if (expire < 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
            return;
        }

        expire--;
        super.move();

    }
}
