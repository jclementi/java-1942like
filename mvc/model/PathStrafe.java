package _08final.mvc.model;

public class PathStrafe extends PathTemplate {
    public PathStrafe() {
        int[][] dests = {
                {800, 200},
                {600, 200},
                {400, 200},
                {200, 200},

                {200, 300},
                {400, 300},
                {600, 300},
                {800, 300}
        };
        ps = dests;
        os = new int[] {
                90, 90, 90, 90,
                90, 90, 90, 90
        };
        ss = new int[] {
                5, 5, 5, 5,
                5, 5, 5, 5
        };
        fs = new boolean[] {
                true, true, true, true,
                true, true, true, true
        };
    }
}
