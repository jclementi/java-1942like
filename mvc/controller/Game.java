package _08final.mvc.controller;

import _08final.mvc.model.*;
import _08final.mvc.view.GamePanel;
import _08final.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.PathIterator;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import static java.awt.event.KeyEvent.*;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(1100, 900); //the dimension of the game.
	private GamePanel gmpPanel;
	public static Random R = new Random();
	public final static int ANI_DELAY = 45;
	private Thread thrAnim;
	private int nLevel = 1;
	private int nTick = 0;
	private int levelStart = 0;
	private LinkedList<EnemySpawnOrder> levelScript = new LevelOne().es;

	private boolean bMuted = true;
	

	private final int
	PAUSE = 80, // p key
	LEFT = VK_LEFT, // rotate left; left arrow
	RIGHT = VK_RIGHT, // rotate right; right arrow
	UP = VK_UP, // thrust; up arrow
	DOWN = VK_DOWN,
	FIRE = VK_SPACE, // space key
	MUTE = 77, // m-key mute
	START = 83, // s key
	QUIT = 81 // q key
	// HYPER = 68, 					// d key
	// SHIELD = 65, 				// a key arrow
	// NUM_ENTER = 10, 				// hyp
	// SPECIAL = 70 				// fire special weapon;  F key
	;

	private Clip clpThrust;
	private Clip clpMusicBackground;

	private static final int SPAWN_NEW_SHIP_FLOATER = 1200;



	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {
		gmpPanel = new GamePanel(DIM);
		gmpPanel.addKeyListener(this);
		clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
		clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");
	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
					public void run() {
						try {
							Game game = new Game(); // construct itself
							game.fireUpAnimThread();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void fireUpAnimThread() { // called initially
		if (thrAnim == null) {
			thrAnim = new Thread(this); // pass the thread a runnable object (this)
			thrAnim.start();
		}
	}

	// implements runnable - must have run method
	public void run() {

		// lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		thrAnim.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// this thread animates the scene
		while (Thread.currentThread() == thrAnim) {
			tick();
			spawnNewShipFloater();
			spawnEnemies();
			gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must 
														// surround the sleep() in a try/catch block
														// this simply controls delay time between 
														// the frames of the animation

			checkCollisions();

			//we are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists while iterating them above
			while(!CommandCenter.getInstance().getOpsList().isEmpty()){
				CollisionOp cop =  CommandCenter.getInstance().getOpsList().dequeue();
				Movable mov = cop.getMovable();
				CollisionOp.Operation operation = cop.getOperation();

				switch (mov.getTeam()){
					case FOE:
						if (operation == CollisionOp.Operation.ADD){
							CommandCenter.getInstance().getMovFoes().add(mov);
						} else {
							CommandCenter.getInstance().getMovFoes().remove(mov);
						}

						break;
					case FRIEND:
						if (operation == CollisionOp.Operation.ADD){
							CommandCenter.getInstance().getMovFriends().add(mov);
						} else {
							CommandCenter.getInstance().getMovFriends().remove(mov);
						}
						break;

					case FLOATER:
						if (operation == CollisionOp.Operation.ADD){
							CommandCenter.getInstance().getMovFloaters().add(mov);
						} else {
							CommandCenter.getInstance().getMovFloaters().remove(mov);
						}
						break;

					case DEBRIS:
						if (operation == CollisionOp.Operation.ADD){
							CommandCenter.getInstance().getMovDebris().add(mov);
						} else {
							CommandCenter.getInstance().getMovDebris().remove(mov);
						}
						break;


				}

			}

			// resolve damage on enemies
			for (Movable mov : CommandCenter.getInstance().getMovFoes()) {
				if (mov instanceof Enemy) {
					Enemy e = (Enemy) mov;
					e.resolveDamage();
				}
			}
			//a request to the JVM is made every frame to garbage collect, however, the JVM will choose when and how to do this
			System.gc();

			//this might be a good place to check if the level is clear (no more foes)
			//if the level is clear then spawn some big asteroids -- the number of asteroids 
			//should increase with the level. 
			checkNewLevel();

			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// just skip this frame -- no big deal
				continue;
			}
		} // end while
	} // end run

	/** new checkCollisions method
	 * handles multisprites
	 * allows collisions between non-circular objects
	 * checks collisions with separating axis theorem
	 */
	private boolean areColliding(Polygon a, Polygon b) {
		PathIterator main_pi;
		Polygon main, other;
		if (a.npoints <= b.npoints) {
			main_pi = a.getPathIterator(null);
			main = a;
			other = b;
		} else {
			main_pi = b.getPathIterator(null);
			main = b;
			other = a;
		}

		// interesting c-style pattern
		// we need to pass these arrays into the accessors of the iterators
		// so they can be filled with points
		double[] first_point;
		double[] next_point = new double[6];
		double[] current_point = new double[6];
		double[] point_in_main = new double[6];
		double[] point_in_other = new double[6];
		PathIterator points_main;
		PathIterator points_other;

		double dist_x, dist_y, length, proj;
		double min_main, max_main;
		double min_other, max_other;

		main_pi.currentSegment(current_point);
		first_point = current_point.clone();
		main_pi.next();
		for (int i = 0; i < main.npoints; i++) {
			min_main = Double.MAX_VALUE; max_main = -Double.MAX_VALUE;
			min_other = Double.MAX_VALUE; max_other = -Double.MAX_VALUE;

			// determine unit vector for projection
			main_pi.currentSegment(next_point);

			dist_x = next_point[0] - current_point[0];
			dist_y = next_point[1] - current_point[1];

			if (i == main.npoints-1) {
				dist_x = first_point[0] - next_point[0];
				dist_y = first_point[1] - next_point[1];
			}
			length = Math.sqrt((dist_x * dist_x) + (dist_y * dist_y));
			dist_x /= length;
			dist_y /= length;

			// perform projection by checking every corner's distance, keeping extremes
			points_main = main.getPathIterator(null);
			while(!points_main.isDone()) {
				points_main.currentSegment(point_in_main);
				points_main.next();
				proj = point_in_main[0]*dist_x + point_in_main[1]*dist_y;
				if (proj < min_main) { min_main = proj; }
				if (proj > max_main) { max_main = proj; }
			}

			points_other = other.getPathIterator(null);
			while(!points_other.isDone()) {
				points_other.currentSegment(point_in_other);
				points_other.next();
				proj = point_in_other[0]*dist_x + point_in_other[1]*dist_y;
				if (proj < min_other) { min_other = proj; }
				if (proj > max_other) { max_other = proj; }
			}
			// if the don't overlap, return false
			if ((min_main > max_other) || (min_other > max_main))
				return false;

			// get set for iterating over the next projection axis
			current_point = next_point.clone();
			main_pi.next();
		}

		return true;
	}

	private void checkCollisions() {
		for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
			// want to exempt a protected falcon
			if(movFriend instanceof Falcon) {
				if (((Falcon) movFriend).getProtected()) {
					continue;
				}
			}
			ArrayList<Sprite> friends = new ArrayList<>();
			if (movFriend instanceof MultiSprite) {
				for (ConstituentSprite cs : ((MultiSprite) movFriend).getSprites()) {
					friends.add(cs.getSprite());
				}
			} else {
				friends.add((Sprite) movFriend);
			}

			for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
				ArrayList<Sprite> foes = new ArrayList<>();
				if (movFoe instanceof Enemy) {
					Enemy e = (Enemy) movFoe;
					for (ConstituentSprite cs : e.getRepresentation().getSprites()) {
						foes.add(cs.getSprite());
					}
				} else if (movFoe instanceof Shot) {
					Shot s = (Shot) movFoe;
					for (ConstituentSprite cs : s.getRepresentation().getSprites()) {
						foes.add(cs.getSprite());
					}
				} else {
					foes.add((Sprite) movFoe);
				}

				// check to see if any of them are colliding
				for (Sprite friend : friends) {
					for (Sprite foe : foes) {
						if (areColliding(friend.getHitbox(), foe.getHitbox())) {
							// deal damage
							friend.setHealth(friend.getHealth() - foe.getStrength());
							foe.setHealth(foe.getHealth() - friend.getStrength());

							if ((movFriend instanceof Falcon) ){
								Falcon fal = CommandCenter.getInstance().getFalcon();
								if (!(fal.getHealth() > 0)){
									CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
									CommandCenter.getInstance().getOpsList().enqueue(new Debris(
											fal.getCenter(), fal.getDeltaX(), fal.getDeltaY(), fal.getRadius(), 20
									), CollisionOp.Operation.ADD);
									CommandCenter.getInstance().spawnFalcon(false);
								} else {
									fal.flash(3,2);
								}
							} else {
								CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
							}//end else // <- wtf is this comment


							if (movFoe instanceof Shot) {
								killFoe(movFoe);
							} else {
								foe.flash(3,2);
							}
							Sound.playSound("kapow.wav");
						}
					}
				}//end if 
			}//end inner for
		}//end outer for


		//check for collisions between falcon and floaters
		if (CommandCenter.getInstance().getFalcon() != null){
			Falcon fal = CommandCenter.getInstance().getFalcon();

			for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
				//detect collision
				if (movFloater instanceof Sprite) {
					Sprite fl = (Sprite) movFloater;
					if (areColliding(fl.getHitbox(), fal.getHitbox())) {
						CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);
						CommandCenter.getInstance().incNumFalcons();
						Sound.playSound("pacman_eatghost.wav");

					}
				}//end if 
			}//end inner for
		}//end if not null
	}



	private void killFoe(Movable movFoe) {
		/*if (movFoe instanceof Asteroid){

			//we know this is an Asteroid, so we can cast without threat of ClassCastException
			Asteroid astExploded = (Asteroid)movFoe;
			//big asteroid 
			if(astExploded.getSize() == 0){
				//spawn two medium Asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);

			} 
			//medium size aseroid exploded
			else if(astExploded.getSize() == 1){
				//spawn three small Asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);

			}

		} */

		//remove the original Foe
		CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);

	}

	//some methods for timing events in the game,
	//such as the appearance of UFOs, floaters (power-ups), etc. 
	public void tick() {
		if (!CommandCenter.getInstance().isPlaying()) { return; }
		if (nTick == Integer.MAX_VALUE) {
			nTick = 0;
		} else {
			nTick++;
		}
	}

	public int getTick() {
		return nTick;
	}

	private void spawnNewShipFloater() {
		if (nTick % (SPAWN_NEW_SHIP_FLOATER - nLevel * 7) == 0) {
			CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(), CollisionOp.Operation.ADD);
		}
	}

	private void spawnEnemies() {
		if (levelScript.isEmpty()) { return; }

		EnemySpawnOrder eso = levelScript.getFirst();

		if (eso.time <= nTick - levelStart) {
			CommandCenter.getInstance().getOpsList().enqueue(new Enemy(eso.et), CollisionOp.Operation.ADD);
			levelScript.removeFirst();
		}
	}

	// Called when user presses 's'
	private void startGame() {
		CommandCenter.getInstance().clearAll();
		CommandCenter.getInstance().initGame();
		CommandCenter.getInstance().setLevel(0);
		CommandCenter.getInstance().setPlaying(true);
		CommandCenter.getInstance().setPaused(false);
		levelScript = new LevelOne().es;
		//if (!bMuted)
		   // clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
	}


	//this method spawns new asteroids
	private void spawnAsteroids(int nNum) {
		for (int nC = 0; nC < nNum; nC++) {
			//Asteroids with size of zero are big
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

		}
	}
	

	private boolean isLevelClear(){
		return (levelScript.isEmpty() && CommandCenter.getInstance().getMovFoes().isEmpty());
	}


	private void checkNewLevel(){
		
		if (isLevelClear()){
			if (CommandCenter.getInstance().getFalcon() != null) {
				CommandCenter.getInstance().getFalcon().setProtected(true);
			}

			levelStart = nTick;
			int level = CommandCenter.getInstance().getLevel() + 1;
			CommandCenter.getInstance().setLevel(level);
			switch (level) {
				case 2: levelScript = new LevelTwo().es; break;
				case 3: levelScript = new LevelThree().es; break;
				case 4: CommandCenter.getInstance().setPlaying(false);
			}

		}
	}

	// Varargs for stopping looping-music-clips
	private static void stopLoopingSounds(Clip... clpClips) {
		for (Clip clp : clpClips) {
			clp.stop();
		}
	}

	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();

		if (nKey == START && !CommandCenter.getInstance().isPlaying())
			startGame();

		if (fal != null) {

			switch (nKey) {
				case PAUSE:
					CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
					if (CommandCenter.getInstance().isPaused()) {
						//stopLoopingSounds(clpMusicBackground, clpThrust);
					} else {
						clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
					}
					break;
				case QUIT:
					System.exit(0);
					break;
				case UP:
					fal.accelerateForward();
					/*if (!CommandCenter.getInstance().isPaused())
						clpThrust.loop(Clip.LOOP_CONTINUOUSLY);)*/
					break;
				case DOWN:
					fal.accelerateBack();
					break;
				case LEFT:
					fal.accelerateLeft();
					break;
				case RIGHT:
					fal.accelerateRight();
					break;

			// possible future use
			// case KILL:
			// case SHIELD:
			// case NUM_ENTER:

				default:
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		int nKey = e.getKeyCode();

		if (fal != null) {
			switch (nKey) {
				case FIRE:
					CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal), CollisionOp.Operation.ADD);
					Sound.playSound("laser.wav");
					break;

				//special is a special weapon, current it just fires the cruise missile.
				/*case SPECIAL:
					CommandCenter.getInstance().getOpsList().enqueue(new Cruise(fal), CollisionOp.Operation.ADD);
					//Sound.playSound("laser.wav");
					break;*/

				case LEFT:
					fal.killAccelerationLeft();
	//				fal.stopRotating();
					//fal.setDeltaX(0);
					break;
				case RIGHT:
					fal.killAccelerationRight();
	//				fal.stopRotating();
					//fal.setDeltaX(0);
					break;
				case UP:
					fal.killAccelerationForward();
	//				fal.thrustOff();
	//				clpThrust.stop();
					//fal.setDeltaY(0);
					break;
				case DOWN:
					fal.killAccelerationBack();
					//fal.setDeltaY(0);
					break;
				case MUTE:
					if (!bMuted){
						//stopLoopingSounds(clpMusicBackground);
						bMuted = !bMuted;
					}
					else {
//						clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
						bMuted = !bMuted;
					}
					break;


				default:
					break;
			}
		}
	}

	@Override
	// Just need it b/c of KeyListener implementation
	public void keyTyped(KeyEvent e) {
	}

}


