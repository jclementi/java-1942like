java 1942-like
==============

This game was built for an introductory Java course as part of the Master Program of Computer Science at the University of Chicago. It's built to imitate the classic arcade game "1942".

The game uses a modular system of classes to build enemies from an assortment of different weapons, sprites, shapes, and flight patterns. Enemies are built by subclassing the `EnemyTemplate` class, such as in `EnemyTriangleShooter`. The resulting class accepts a list of `PathTemplate`s, each of which is a set of instructions for movement. By building sets of paths and enemies, levels could be easily scripted by creating enemies with movement patterns, then putting them in a list with when they should appear.
